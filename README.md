# Rutas en angular

Manejar rutas en angular es muy sencillo. Debemos recordar que las rutas son muy importantes en angular ya que se crean aplicaciones de una sola página.

En esta entrada hablaremos de rutas sencillas es decir no hablaremos rutas con lazy loading. Para ello utilizaremos el proyecto de la entrada anterior.

## Creando componentes
Para crear componentes podemos hacerlo de forma manual es decir crear una carpeta con archivos html, css y ts o se puede generar un componente automaticamente utilizando el siguiente comando en una terminal dentro del proyecto.
```
ng g component nombre-componente
```

Si se necesita mas ayuda de parametros para generar componentes debe utilizarse

```
ng g component -h
```

En mi caso cree dos componentes uno llamado *login* y el otro *registro*
![](imagenes/componentesdirectorio.png)

tambien crearemos un archivo routing.module.ts dentro de la carpeta app.

Dentro del archivo ponemos el siguiente código e importamos lo necesario.
``` TypeScript
const ruta: Route[] =[
    {
        path:'login',
        component: LoginComponent
    },
    {
        path:'registro',
        component: RegistroComponent
    }
]

@NgModule({
    imports:[RouterModule.forRoot(ruta)],
    exports:[RouterModule]
})
export class RoutingModule{}
```
como podemos ver en la variable rutas del tipo Route (importado desde @angular/router) se declaran en un json la ruta y el component.

Dentro del decorador NgModule especificamos el nombre de la ruta y exportamos la ruta.

Agregamos este archivo en la parte de imports de app.module.ts de esta manera.

![](imagenes/routingmodule.png)

una vez hecho esto vamos al archivo app.component.html y reemplazamos todo lo que haya ahí con:
``` Html
<router-outlet></router-outlet>
```

de esta forma.

![](imagenes/routeroutlet.png)

y eso es todo, levantamos el servidor con:
```
ng s
```
la aplicacion se levanta en el puerto 4200

**Login**

![](imagenes/loginworks.png)

**Registro**


![](imagenes/registroworks.png)

<a href="https://twitter.com/atpsito" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @atpsito </a><br>
<a href="https://www.linkedin.com/in/alexander-tigselema-ba1443124/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Alexander Tigselema</a><br>
<a href="https://www.instagram.com/atpsito" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> atpsito</a><br>