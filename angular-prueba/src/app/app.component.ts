import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  items: MenuItem[];

  ngOnInit() {
    this.items = [{
      label: 'File',
      items: [
        { label: 'New' },
        { label: 'Open' }
      ]
    },
    {
      label: 'Edit',
      items: [
        { label: 'Undo' },
        { label: 'Redo' }
      ]
    }];
  }
}
