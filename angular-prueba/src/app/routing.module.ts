import { NgModule } from "@angular/core";
import { RouterModule, Route  } from "@angular/router"
import { LoginComponent } from "./login/login.component";
import { RegistroComponent } from "./registro/registro.component";

const ruta: Route[] =[
    {
        path:'login',
        component: LoginComponent
    },
    {
        path:'registro',
        component: RegistroComponent
    }
]

@NgModule({
    imports:[RouterModule.forRoot(ruta)],
    exports:[RouterModule]
})
export class RoutingModule{}